#!/bin/bash

# Directory structure
DIRECTORIES=("tasks" "tasks/install" "meta" "defaults" "templates" "molecule" "handlers")

# Create directories
for dir in "${DIRECTORIES[@]}"; do
    mkdir -p $dir
done

# Create defaults file
tee defaults/main.yaml <<EOF
---
# Defaults
kubernetes_version: 1.27.1-00
kubernetes_user: akalaj
EOF

# Create handlers main
tee handlers/main.yaml <<EOF
# handlers file
---
-
EOF

# Create meta main
tee meta/main.yaml <<EOF
# meta file
---
-
EOF
# Create tasks main & install yaml
tee tasks/main.yaml <<EOF
---
# - name: Include OS Family Relevant Tasks
#   ansible.builtin.include_tasks: "install/{{ ansible_os_family }}.yaml"

# - name: Example task
#   ansible.builtin.include_tasks: "k8s-base-install.yaml"
EOF

tee tasks/install/Debian.yaml <<EOF
---
- name: Installing Debian dependencies for apt
  ansible.builtin.apt:
    name:
      - gpg
      - vim
      - git
      - jq
      - wget
      - curl
      - apt-transport-https
      - software-properties-common
      - lsb-release
      - ca-certificates
      - htop
      - tree
      - net-tools
      - mlocate
      - bash-completion
      - python3-pip
      - python3-yaml
    state: present
    update_cache: true
  when:
    - ansible_os_family == 'Debian'
EOF

# setup template file
tee templates/example.yaml.j2 <<EOF
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
kubernetesVersion: 1.27.1
controlPlaneEndpoint: "k8scp:6443"
networking:
    podSubnet: 192.168.0.0/16
EOF